package com.jin.study.datastructure;

public class LeetCode {

    public static void main(String[] args) {
        testSingleNumber();
       // testRemoveElement();
        //testRemoveDuplicates();
    }

    public static void testSingleNumber(){
        int[] nums = {4,33,2,1,1,2,4};
        System.out.println(singleNumber(nums));
    }

    /**
     * 给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素
     *
     * 使用异或
     *  相同的两个数异或则为0
     *
     * @param nums
     * @return
     */
    public static int singleNumber(int[] nums){
        int ans = nums[0];
        if (nums.length > 1) {
            for (int i = 1; i < nums.length; i++) {
                ans = ans ^ nums[i];
            }
        }
        return ans;
    }


    public static void testRemoveElement(){
        int[] a = {1,2,3,3,4,6,3,5,6,7,7,3,8};
        int l = removeElement3(a,3) ;
        System.out.println(l);
        for(int i = 0 ;i<l;i++){
            System.out.print(a[i]+",");
        }
        System.out.println();
        for(int i = 0 ;i<a.length;i++){
            System.out.print(a[i]+",");
        }

    }
    public static void testRemoveDuplicates(){
        int[] a = {1,2,3,3,3,4,5,6,6,6,6,7,7,7,8,8,8};
        int l = removeDuplicates1(a) ;
        System.out.println(l);
        for(int i = 0 ;i<l;i++){
            System.out.print(a[i]+",");
        }
        System.out.println();
        for(int i = 0 ;i<a.length;i++){
            System.out.print(a[i]+",");
        }

    }


    /**
     * 给定一个数组 nums 和一个值 val，你需要原地移除所有数值等于 val 的元素，返回移除后数组的新长度。
     不要使用额外的数组空间，你必须在原地修改输入数组并在使用 O(1) 额外空间的条件下完成。
     元素的顺序可以改变。你不需要考虑数组中超出新长度后面的元素。

     来源：力扣（LeetCode）
     链接：https://leetcode-cn.com/problems/remove-element
     著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     * @param nums
     * @param val
     * @return
     */
    public static int removeElement(int[] nums, int val) {
        if(nums == null){
            return 0 ;
        }
        if(nums.length==1){
            return nums[0]==val?0:1;
        }
        int len = nums.length;
        for(int i =0 ;i< nums.length;i++){
            if(nums[i] == val){
                len --;
                for(int j = i ;j< nums.length-1;j++){
                    nums[j] = nums[j+1];
                }
                i--;
            }
        }
        return len ;
    }

    public static int removeElement1(int[] nums, int val) {
        int i = -1;
        for (int j = 0; j < nums.length; j++) {
            if (nums[j] != val) {
                nums[++i] = nums[j];
            }
        }
        return i + 1;
    }

    public static int removeElement3(int[] nums, int val) {
        int i = nums.length;
        int j = 0 ;
        while ( j < i ) {
            if (nums[j] == val) {
                nums[j] = nums[--i];
            }else{
                j++;
            }
        }
        return i ;
    }


    /**
     * 给定一个排序数组，原地删除重复出现的数字，使得每个数字只出现一次，返回移除后数组的长度
     * 不使用额外的空间
     *
     * 有序，正序吧，则代表着前一个元素必定比后一个元素大才可以满足条件
     * [i]==[i+1] ,dupliate num ,the next element replace [i+1],len-1
     * [i]<[i+1],next
     * [i] > [i+1] is impossible
     * @param nums
     * @return
     */
    public static int removeDuplicates(int[] nums){
        int baseLen =  nums.length;
        int len = nums.length;
        int j = 0 ;
        for(int i = 1;i < baseLen;i++){
            if(j != 0){
                if(nums[j] < nums[i]){
                    nums[j] = nums[++j]= nums[i];
                }else{
                    len--;
                }
            }else if(nums[i-1] == nums[i]){
                j = i ;
                len--;
            }
        }
        return len;
    }

    public static int removeDuplicates1(int[] nums) {
        if(nums == null){
            return 0;
        }
        if( nums.length == 1){
            return 1;
        }
        int i = 0,j =1;
        while(j<nums.length){
            if(nums[i]!=nums[j]) {
                nums[++i] = nums[j];
            }
            j++;
        }
        return i+1;
    }



}
