package com.jin.study.datastructure;

/**
 * all kinds of simple sort algorithmic
 *
 *
 *
 */
public class SimpleSort {

    /**
     * 冒泡排序
     * 原理：比较两个相邻的元素，将值大的元素交换至右端
     * 思路：依次比较相邻的两个数，将小数放在前面，大数放在后面
     *      第一趟比较，
     *          比较第一和第二，小数前大数后；再比较2和3,3和4,4和5.如此，第一趟比较完成后，最后一个数一定是数组中最大的一个数，所以第二趟比较的时候最后一个数不参与比较
     *       第二趟比较完成后，倒数第二个数也一定是数组中第二大的数，所以第三趟比较的时候最后两个数不参数比较
     *       类推，每一趟比较次数-1
     *
     *       最好 O(n) ,平均和最坏O（n~2）
     *
     * @param data
     */
    public static void bubble(int[] data){
        int size = data.length ;
        int tem ;
        for(int i = size-1;i>0;i--){
            for(int j=0;j<i;j++){
                if(data[j] > data[j+1]){
                    tem = data[j];
                    data[j] = data[j+1];
                    data[j+1] = tem ;
                }
            }
        }
    }

    /**
     * 优化冒泡排序，利用中间变量标识是否有数据交换，若没有，则直接结束
     * @param data
     */
    public static void bubble2(int[] data){
        int size = data.length;
        int tem;
        boolean flag  ;
        for(int i = size - 1 ;i > 0 ;i--){
            flag = false ;
            for(int j = 0 ;j<i;j++){
                if(data[j] > data[j+1]){
                    tem = data[j] ;
                    data[j] = data[j+1];
                    data[j+1] = tem ;
                    flag = true ;
                }
            }
            if(!flag){
                break;
            }
        }
    }

    /**
     * 选择排序
     * 原理：每一趟从待排序的记录中选出最小的元素，顺序放在已排好序的序列最后，直到全部记录排序完毕
     * @param data
     */
    public static void selectionSort(int[] data){
        int tem ;
        int size = data.length;
        for(int i =0 ;i < size - 1 ;i++){
            for(int j = i+1;j<size ;j++){
                if(data[i] > data[j]){
                    tem = data[i];
                    data[i] = data[j];
                    data[j] = tem ;
                }
            }
        }
    }

    /**
     * 选择排序的优化
     * @param data
     */
    public static void selectionSort2(int[] data){
        int size = data.length;
        for(int i =0 ;i < size - 1 ;i++){
            int k = i ;
            for(int j = k+1;j<size ;j++){
                if(data[k] > data[j]){
                   k = j ; // 记下目前找到的最小值的位置
                }
            }

            if(i != k){
                int tmp = data[i];
                data[i] = data[k];
                data[k] = tmp ;
            }
        }
    }

    public static void insertSort(int[] data){
        int tem ;
        int size = data.length ;
        int j ;
        for(int i= 1 ;i < size ;i++){
            tem = data[i];
            j = i - 1 ;
            while(j > 0 && tem < data[j]){
                data[j+1] = data[j];
                j--;
            }
            data[j-1] = tem ;
        }
    }

    public static void shellSort(int[] data){
        int tem ;
        int jmp ;
        int size = data.length ;
        int j ;
        jmp = size/2 ;
        while (jmp!=0){
            for(int i = jmp;i<size;i++){
                tem = data[i] ;
                j = i - jmp ;
                while (j>=0&&tem<data[j]){
                    data[j+jmp] = data[j];
                    j = j -jmp ;

                }
                data[jmp+j] = tem ;
            }
            jmp = jmp/2 ;
        }
    }


    public static void quickSort2(int[] data,int left ,int right ){
        int tem ;
        int lf_idx ;
        int rg_idx;
        if(left < right ){
            lf_idx = left + 1 ;
            rg_idx = right ;
            while (left < right){
                while (true){
                    for(int i = left + 1;i<=right;i++){
                        if(data[i] >= data[left]){
                            lf_idx = i;
                            break;
                        }
                        lf_idx++;
                    }
                    for (int j = right ;j>=left+1;j--){
                        if(data[j] <= data[left]){
                            rg_idx = j ;
                            break;
                        }
                        rg_idx--;
                    }
                    if(lf_idx<rg_idx){
                        tem = data[lf_idx];
                        data[lf_idx] = data[rg_idx];
                        data[rg_idx] = tem ;
                    }else{
                        break;
                    }
                }
                if(lf_idx >= rg_idx){
                    tem = data[left];
                    data[left] = data[rg_idx];
                    data[rg_idx] = tem ;
                    quickSort2(data,left,rg_idx-1);
                    quickSort2(data,rg_idx+1,right);
                }

            }
;
        }
    }

    public static void buildHeap(int[] a){
        int heapSize = a.length ;
        int filter = heapSize/2;
        for(int i = filter-1 ;i>=0 ;i--){
            heapAdjust(a,i,heapSize);
        }
    }
    public static void heapAdjust(int[] arr,int i ,int heapSize){
        int tem = arr[i];
        int index = 2*i+1 ;
        while (index < heapSize){
            if (index + 1 < heapSize && arr[index] < arr[index+1]) {
                index = index +1 ;
            }
            if(arr[i] < arr[index]){
                arr[i] = arr[index];
                i = index ;
                index = 2*i+1;

            }else {
                break;
            }
            arr[i]=tem;
        }
    }

    public static void heapSort(int[] a){
        int heapSize = a.length ;
        for(int i = heapSize - 1 ;i> 0;i--){
            int tem = a[0] ;
            a[0] = a[i] ;
            a[i] = tem ;
            heapAdjust(a,0,i);
        }
    }

    public static int[] mergeSort(int[] arr){
        int len = arr.length ;
        int[] result = new int[len];
        int block,start ;
        for(block =1 ;block < len ;block *=2){
            for(start = 0;start < len ;start +=2*block){
                int low = start ;
                int mid = (start + block) < len ?(start+block):len ;
                int high = (start + 2* block)<len? (start + 2* block):len ;
                int start1 = low ,end1 = mid ;
                int start2 = mid ,end2 = high;
                while (start1 < end1 && start2 < end2){
                    result[low++] = arr[start1] < arr[start2]?arr[start1++] : arr[start2++];

                }
                while (start1<end1){
                    result[low++] = arr[start1++];

                }

                while (start2 < end2){
                    result[low++] = arr[start2++] ;
                }
            }
            int[] tem = arr ;
            arr = result ;
            result = tem ;


        }
        result = arr ;
        return result ;
    }

    /**
     * 基数排序，假设每个数不超过三位
     * @param data
     */
    public static void radixSort(int[] data){
        int size = data.length,k,n,m;
        for(n =1 ;n<=100;n=n*10){
            int [][] tem = new int[10][100];
            for(int i=0;i<size;i++){
                m = (data[i]/n)%10 ;
                tem[m][i] = data[i];
            }

            k=0;
            for(int i=0;i<10;i++){
                for(int j=0;j<size;j++){
                    if(tem[i][j]!=0){
                        data[k] = tem[i][j];
                        k++ ;
                    }
                }
            }
        }
    }

}
